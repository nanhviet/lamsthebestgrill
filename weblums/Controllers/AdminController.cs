﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Transactions;
using System.Web;
using System.Web.Mvc;
using System.Web.Security;
using DotNetOpenAuth.AspNet;
using Microsoft.Web.WebPages.OAuth;
using WebMatrix.WebData;
using weblums.Filters;
using weblums.Models;
using weblums.Utils;
using System.IO;

namespace weblums.Controllers
{
    [Authorize]
    [InitializeSimpleMembership]
    public class AdminController : Controller
    {
        //
        // GET: /Admin/admin

        public ActionResult Index()
        {
            var model = new DashboardModel();

            using (var context = new AdminContext())
            {
                var dictionaries = context.Dictionaries.ToList();

                //descriptions
                model.WelcomeDescription = dictionaries.Single((item) => item.Key == "Welcome").Value;
                model.SpecialtiesDescription = dictionaries.Single((item) => item.Key == "Specialties").Value;
                model.MenuDescription = dictionaries.Single((item) => item.Key == "Menu").Value;
                model.ReservationDescription = dictionaries.Single((item) => item.Key == "Reservation").Value;
                model.ContactDescription = dictionaries.Single((item) => item.Key == "Contact").Value;

                //slides
                model.Slide1 = dictionaries.Single((item) => item.Key == "Slide1").Value;
                model.Slide2 = dictionaries.Single((item) => item.Key == "Slide2").Value;
                model.Slide3 = dictionaries.Single((item) => item.Key == "Slide3").Value;
            }

            return View(model);
        }

        //
        // GET: /Admin/notification

        public ActionResult Notification()
        {
            var reservations = new List<ReservationModel>();
            var messages = new List<MessageModel>();

            using (var context = new AdminContext())
            {
                foreach (var reservation in context.Reservations.Where((item) => item.isRead == null || item.isRead.Value == false).ToList())
                {
                    var item = new ReservationModel();
                    item.Id = reservation.Id;
                    item.Name = reservation.Name;
                    item.Email = reservation.Email;
                    item.isRead = reservation.isRead.HasValue ? reservation.isRead.Value : false;
                    item.isConfirmed = reservation.isConfirmed.HasValue ? reservation.isConfirmed.Value : false;
                    item.Phone = reservation.Phone;
                    item.Size = reservation.Size;
                    item.Date = reservation.Date;
                    item.Time = reservation.Time;
                    item.CreatedDate = reservation.CreatedDate;
                    if (!string.IsNullOrEmpty(reservation.Note))
                    {
                        int length = reservation.Note.Length < 70 ? reservation.Note.Length : 70;
                        item.Note = reservation.Note.Substring(0, length) + "...";
                    }

                    reservations.Add(item);
                }

                foreach (var message in context.Messages.Where((item) => item.isRead == null || item.isRead.Value == false).ToList())
                {
                    var item = new MessageModel();
                    item.Id = message.Id;
                    item.Name = message.Name;
                    item.Email = message.Email;
                    item.isRead = message.isRead.HasValue ? message.isRead.Value : false;
                    item.CreatedDate = message.CreatedDate;
                    if (!string.IsNullOrEmpty(message.Message))
                    {
                        int length = message.Message.Length < 70 ? message.Message.Length : 70;
                        item.Message = message.Message.Substring(0, length) + "...";
                    }

                    messages.Add(item);
                }
            }

            return Json(new { error = false, reservations = reservations.ToArray(), messages = messages.ToArray() }, JsonRequestBehavior.AllowGet);
        }

        //
        // GET: /Admin/reservations

        public ActionResult Reservations()
        {
            IList<ReservationModel> model = new List<ReservationModel>();

            using (var context = new AdminContext())
            {
                foreach (var reservation in context.Reservations.OrderBy((item) => item.isRead).ToList())
                {
                    var item = new ReservationModel();
                    item.Id = reservation.Id;
                    item.Name = reservation.Name;
                    item.Email = reservation.Email;
                    item.isRead = reservation.isRead.HasValue ? reservation.isRead.Value : false;
                    item.isConfirmed = reservation.isConfirmed.HasValue ? reservation.isConfirmed.Value : false;
                    item.Phone = reservation.Phone;
                    item.Size = reservation.Size;
                    item.Date = reservation.Date;
                    item.Time = reservation.Time;
                    item.CreatedDate = reservation.CreatedDate;
                    if (!string.IsNullOrEmpty(reservation.Note))
                    {
                        int length = reservation.Note.Length < 70 ? reservation.Note.Length : 70;
                        item.Note = reservation.Note.Substring(0, length) + "...";
                    }

                    model.Add(item);
                }
            }

            return View(model);
        }

        //
        // GET: /Admin/reservations/edit

        public ActionResult EditReservation(int id)
        {
            ReservationModel model = new ReservationModel();

            using (var context = new AdminContext())
            {
                var reservation = context.Reservations.Single((item) => item.Id == id);
                model.Id = reservation.Id;
                model.Name = reservation.Name;
                model.Email = reservation.Email;
                model.Phone = reservation.Phone;
                model.Size = reservation.Size;
                model.Date = reservation.Date;
                model.Time = reservation.Time;
                model.Note = reservation.Note;
                model.isConfirmed = reservation.isConfirmed.HasValue ? reservation.isConfirmed.Value : false;
                model.isRead = true;

                reservation.isRead = true;
                context.SaveChanges();
            }

            return View("Reservations.Edit", model);
        }

        //
        // PUT: /Admin/reservations/edit

        [HttpPut]
        [ValidateAntiForgeryToken]
        public ActionResult EditReservation(ReservationModel model)
        {
            try
            {
                using (var context = new AdminContext())
                {
                    var reservation = context.Reservations.Single((item) => item.Id == model.Id);
                    reservation.Name = model.Name;
                    reservation.Email = model.Email;
                    reservation.Phone = model.Phone;
                    reservation.Size = model.Size.Value;
                    reservation.Date = model.Date.Value;
                    reservation.Time = model.Time;
                    reservation.Note = model.Note;
                    reservation.isRead = model.isRead;
                    reservation.isConfirmed = model.isConfirmed;

                    context.SaveChanges();
                }

                return Json(new { error = false, message = "Save succeeded." }, JsonRequestBehavior.AllowGet);
            }
            catch (Exception ex)
            {
                return Json(new { error = true, message = "Save failed." }, JsonRequestBehavior.AllowGet);
            }
        }

        //
        // DELETE: /Admin/reservations/delete

        [HttpDelete]
        public ActionResult DeleteReservation(int id)
        {
            try
            {

                using (var context = new AdminContext())
                {
                    var reservation = context.Reservations.Single((item) => item.Id == id);
                    context.Reservations.Remove(reservation);
                    context.SaveChanges();
                }

                return Json(new { error = false, message = "Delete succeeded." }, JsonRequestBehavior.AllowGet);
            }
            catch (Exception ex)
            {
                return Json(new { error = true, message = "Delete failed." }, JsonRequestBehavior.AllowGet);
            }
        }

        //
        // GET: /Admin/messages

        public ActionResult Messages()
        {
            IList<MessageModel> model = new List<MessageModel>();

            using (var context = new AdminContext())
            {
                foreach (var message in context.Messages.OrderBy((item) => item.isRead).ToList())
                {
                    var item = new MessageModel();
                    item.Id = message.Id;
                    item.Name = message.Name;
                    item.Email = message.Email;
                    item.isRead = message.isRead.HasValue ? message.isRead.Value : false;
                    item.CreatedDate = message.CreatedDate;
                    if (!string.IsNullOrEmpty(message.Message))
                    {
                        int length = message.Message.Length < 70 ? message.Message.Length : 70;
                        item.Message = message.Message.Substring(0, length) + "...";
                    }

                    model.Add(item);
                }
            }

            return View(model);
        }

        //
        // GET: /Admin/message/view

        public ActionResult ViewMessage(int id)
        {
            MessageModel model = new MessageModel();

            using (var context = new AdminContext())
            {
                var message = context.Messages.Single((item) => item.Id == id);
                model.Id = message.Id;
                model.Name = message.Name;
                model.Email = message.Email;
                model.Message = message.Message;
                model.CreatedDate = message.CreatedDate;
                model.isRead = true;

                message.isRead = true;
                context.SaveChanges();
            }

            return View("Messages.View", model);
        }

        //
        // DELETE: /Admin/message/delete

        [HttpDelete]
        public ActionResult DeleteMessage(int id)
        {
            try
            {

                using (var context = new AdminContext())
                {
                    var message = context.Messages.Single((item) => item.Id == id);
                    context.Messages.Remove(message);
                    context.SaveChanges();
                }

                return Json(new { error = false, message = "Delete succeeded." }, JsonRequestBehavior.AllowGet);
            }
            catch (Exception ex)
            {
                return Json(new { error = true, message = "Delete failed." }, JsonRequestBehavior.AllowGet);
            }
        }

        //
        // GET: /Admin/specialties

        public ActionResult Specialties()
        {
            IList<SpecialtyModel> model = new List<SpecialtyModel>();

            using (var context = new AdminContext())
            {
                foreach(var specialty in context.Specialties.OrderBy((item) => item.DisplayIndex).ToList())
                {
                    var item = new SpecialtyModel();
                    item.Id = specialty.Id;
                    item.Name = specialty.Name;
                    if (!string.IsNullOrEmpty(specialty.Description))
                    {
                        int length = specialty.Description.Length < 70 ? specialty.Description.Length : 70;
                        item.Description = specialty.Description.Substring(0, length);
                    }
                    if (!string.IsNullOrEmpty(specialty.ImageUrl))
                    {
                        item.ImageUrl = specialty.ImageUrl.Replace("/Content/upload/", "");
                    }
                    
                    item.DisplayIndex = specialty.DisplayIndex;

                    model.Add(item);
                }
            }

            return View(model);
        }

        //
        // GET: /Admin/specialties/add

        public ActionResult AddSpecialty()
        {
            SpecialtyModel model = new SpecialtyModel();
            return View("Specialties.Add", model);
        }

        //
        // POST: /Admin/specialties/add

        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult AddSpecialty(SpecialtyModel model)
        {
            try
            {
                string image = null;

                foreach (string upload in Request.Files)
                {
                    if (!Request.Files[upload].HasFile()) continue;
                    string path = AppDomain.CurrentDomain.BaseDirectory + "Content\\upload";
                    string filename = Path.GetFileName(Request.Files[upload].FileName);
                    Request.Files[upload].SaveAs(Path.Combine(path, filename));

                    switch (upload)
                    {
                        case "FileUpload":
                            image = filename;
                            break;
                    }
                }

                using (var context = new AdminContext())
                {
                    var specialty = new Specialties();
                    specialty.Name = model.Name;
                    specialty.Description = model.Description;
                    specialty.ImageUrl = "/Content/upload/" + image;
                    specialty.DisplayIndex = model.DisplayIndex;

                    context.Specialties.Add(specialty);
                    context.SaveChanges();
                }

                return Json(new { error = false, message = "Save succeeded." }, JsonRequestBehavior.AllowGet);
            }
            catch(Exception ex)
            {
                return Json(new { error = true, message = "Save failed." }, JsonRequestBehavior.AllowGet);
            }
        }

        //
        // GET: /Admin/specialties/edit

        public ActionResult EditSpecialty(int id)
        {
            SpecialtyModel model = new SpecialtyModel();

            using (var context = new AdminContext())
            {
                var specialty = context.Specialties.Single((item) => item.Id == id);
                model.Id = specialty.Id;
                model.Name = specialty.Name;
                model.Description = specialty.Description;
                model.ImageUrl = specialty.ImageUrl;
                model.DisplayIndex = specialty.DisplayIndex;
            }
            
            return View("Specialties.Edit", model);
        }

        //
        // PUT: /Admin/specialties/edit

        [HttpPut]
        [ValidateAntiForgeryToken]
        public ActionResult EditSpecialty(SpecialtyModel model)
        {
            try
            {
                string image = null;

                foreach (string upload in Request.Files)
                {
                    if (!Request.Files[upload].HasFile()) continue;
                    string path = AppDomain.CurrentDomain.BaseDirectory + "Content\\upload";
                    string filename = Path.GetFileName(Request.Files[upload].FileName);
                    Request.Files[upload].SaveAs(Path.Combine(path, filename));

                    switch (upload)
                    {
                        case "FileUpload":
                            image = filename;
                            break;
                    }
                }

                using (var context = new AdminContext())
                {
                    var specialty = context.Specialties.Single((item) => item.Id == model.Id);
                    specialty.Name = model.Name;
                    specialty.Description = model.Description;
                    if (image != null)
                    {
                        specialty.ImageUrl = "/Content/upload/" + image;
                    }
                    specialty.DisplayIndex = model.DisplayIndex;

                    context.SaveChanges();
                }

                return Json(new { error = false, message = "Save succeeded." }, JsonRequestBehavior.AllowGet);
            }
            catch (Exception ex)
            {
                return Json(new { error = true, message = "Save failed." }, JsonRequestBehavior.AllowGet);
            }
        }

        //
        // DELETE: /Admin/specialties/delete

        [HttpDelete]
        public ActionResult DeleteSpecialty(int id)
        {
            try
            {

                using (var context = new AdminContext())
                {
                    var specialty = context.Specialties.Single((item) => item.Id == id);
                    context.Specialties.Remove(specialty);
                    context.SaveChanges();
                }

                return Json(new { error = false, message = "Delete succeeded." }, JsonRequestBehavior.AllowGet);
            }
            catch (Exception ex)
            {
                return Json(new { error = true, message = "Delete failed." }, JsonRequestBehavior.AllowGet);
            }
        }

        //
        // GET: /Admin/menu

        public ActionResult Menu()
        {
            IList<MenuModel> model = new List<MenuModel>();

            using (var context = new AdminContext())
            {
                foreach (var menu in context.Menu.ToList())
                {
                    var item = new MenuModel();
                    item.Id = menu.Id;
                    item.Name = menu.Name;
                    if (!string.IsNullOrEmpty(menu.Description))
                    {
                        int length = menu.Description.Length < 70 ? menu.Description.Length : 70;
                        item.Description = menu.Description.Substring(0, length) + "...";
                    }
                    if (!string.IsNullOrEmpty(menu.ImageUrl))
                    {
                        item.ImageUrl = menu.ImageUrl.Replace("/Content/upload/", "");
                    }

                    item.Price = menu.Price;
                    item.IsBreakfast = menu.IsBreakfast;
                    item.IsLunch = menu.IsLunch;
                    item.IsDinner = menu.IsDinner;
                    item.IsKids = menu.IsKids;
                    item.IsBeverage = menu.IsBeverage;
                    item.IsGallery = menu.IsGallery;

                    model.Add(item);
                }
            }

            return View(model);
        }

        //
        // GET: /Admin/menu/add

        public ActionResult AddMenu()
        {
            MenuModel model = new MenuModel();
            return View("Menu.Add", model);
        }

        //
        // POST: /Admin/menu/add

        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult AddMenu(MenuModel model)
        {
            try
            {
                string image = null;

                foreach (string upload in Request.Files)
                {
                    if (!Request.Files[upload].HasFile()) continue;
                    string path = AppDomain.CurrentDomain.BaseDirectory + "Content\\upload";
                    string filename = Path.GetFileName(Request.Files[upload].FileName);
                    Request.Files[upload].SaveAs(Path.Combine(path, filename));

                    switch (upload)
                    {
                        case "FileUpload":
                            image = filename;
                            break;
                    }
                }

                using (var context = new AdminContext())
                {
                    var menu = new Menu();
                    menu.Name = model.Name;
                    menu.Description = model.Description;
                    menu.ImageUrl = "/Content/upload/" + image;
                    menu.Price = model.Price;
                    menu.IsBreakfast = model.IsBreakfast;
                    menu.IsLunch = model.IsLunch;
                    menu.IsDinner = model.IsDinner;
                    menu.IsKids = model.IsKids;
                    menu.IsBeverage = model.IsBeverage;
                    menu.IsGallery = model.IsGallery;

                    context.Menu.Add(menu);
                    context.SaveChanges();
                }

                return Json(new { error = false, message = "Save succeeded." }, JsonRequestBehavior.AllowGet);
            }
            catch (Exception ex)
            {
                return Json(new { error = true, message = "Save failed." }, JsonRequestBehavior.AllowGet);
            }
        }

        //
        // GET: /Admin/menu/edit

        public ActionResult EditMenu(int id)
        {
            MenuModel model = new MenuModel();

            using (var context = new AdminContext())
            {
                var menu = context.Menu.Single((item) => item.Id == id);
                model.Id = menu.Id;
                model.Name = menu.Name;
                model.Description = menu.Description;
                model.ImageUrl = menu.ImageUrl;
                model.Price = menu.Price;
                model.IsBreakfast = menu.IsBreakfast;
                model.IsLunch = menu.IsLunch;
                model.IsDinner = menu.IsDinner;
                model.IsKids = menu.IsKids;
                model.IsBeverage = menu.IsBeverage;
                model.IsGallery = menu.IsGallery;
            }

            return View("Menu.Edit", model);
        }

        //
        // PUT: /Admin/menu/edit

        [HttpPut]
        [ValidateAntiForgeryToken]
        public ActionResult EditMenu(MenuModel model)
        {
            try
            {
                string image = null;

                foreach (string upload in Request.Files)
                {
                    if (!Request.Files[upload].HasFile()) continue;
                    string path = AppDomain.CurrentDomain.BaseDirectory + "Content\\upload";
                    string filename = Path.GetFileName(Request.Files[upload].FileName);
                    Request.Files[upload].SaveAs(Path.Combine(path, filename));

                    switch (upload)
                    {
                        case "FileUpload":
                            image = filename;
                            break;
                    }
                }

                using (var context = new AdminContext())
                {
                    var menu = context.Menu.Single((item) => item.Id == model.Id);
                    menu.Name = model.Name;
                    menu.Description = model.Description;
                    if (image != null)
                    {
                        menu.ImageUrl = "/Content/upload/" + image;
                    }
                    menu.Price = model.Price;
                    menu.IsBreakfast = model.IsBreakfast;
                    menu.IsLunch = model.IsLunch;
                    menu.IsDinner = model.IsDinner;
                    menu.IsKids = model.IsKids;
                    menu.IsBeverage = model.IsBeverage;
                    menu.IsGallery = model.IsGallery;

                    context.SaveChanges();
                }

                return Json(new { error = false, message = "Save succeeded." }, JsonRequestBehavior.AllowGet);
            }
            catch (Exception ex)
            {
                return Json(new { error = true, message = "Save failed." }, JsonRequestBehavior.AllowGet);
            }
        }

        //
        // DELETE: /Admin/mmenu/delete

        [HttpDelete]
        public ActionResult DeleteMenu(int id)
        {
            try
            {

                using (var context = new AdminContext())
                {
                    var menu = context.Menu.Single((item) => item.Id == id);
                    context.Menu.Remove(menu);
                    context.SaveChanges();
                }

                return Json(new { error = false, message = "Delete succeeded." }, JsonRequestBehavior.AllowGet);
            }
            catch (Exception ex)
            {
                return Json(new { error = true, message = "Delete failed." }, JsonRequestBehavior.AllowGet);
            }
        }

        //
        // GET: /Admin/Login

        [AllowAnonymous]
        public ActionResult Login(string returnUrl)
        {
            ViewBag.ReturnUrl = returnUrl;
            return View();
        }

        //
        // POST: /Admin/Login

        [HttpPost]
        [AllowAnonymous]
        [ValidateAntiForgeryToken]
        public ActionResult Login(LoginModel model, string returnUrl)
        {
            if (ModelState.IsValid && WebSecurity.Login(model.UserName, model.Password, persistCookie: model.RememberMe))
            {
                return RedirectToLocal(returnUrl);
            }

            // If we got this far, something failed, redisplay form
            ModelState.AddModelError("", "The user name or password provided is incorrect.");
            return View(model);
        }

        //
        // POST: /Admin/LogOff

        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult LogOff()
        {
            WebSecurity.Logout();

            return RedirectToAction("Index", "Home");
        }

        //
        // POST: /Admin/Dashboard

        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Dashboard(DashboardModel model)
        {
            string slide1 = null;
            string slide2 = null;
            string slide3 = null;

            foreach (string upload in Request.Files)
            {
                if (!Request.Files[upload].HasFile()) continue;
                string path = AppDomain.CurrentDomain.BaseDirectory + "Content\\upload";
                string filename = Path.GetFileName(Request.Files[upload].FileName);
                Request.Files[upload].SaveAs(Path.Combine(path, filename));

                switch (upload)
                {
                    case "FileUpload1":
                        slide1 = filename;
                        break;
                    case "FileUpload2":
                        slide2 = filename;
                        break;
                    case "FileUpload3":
                        slide3 = filename;
                        break;
                }
            }

            using (var context = new AdminContext())
            {
                var dictionaries = context.Dictionaries.ToList();
                dictionaries.Single((item) => item.Key == "Welcome").Value = model.WelcomeDescription;
                dictionaries.Single((item) => item.Key == "Specialties").Value = model.SpecialtiesDescription;
                dictionaries.Single((item) => item.Key == "Menu").Value = model.MenuDescription;
                dictionaries.Single((item) => item.Key == "Reservation").Value = model.ReservationDescription;
                dictionaries.Single((item) => item.Key == "Contact").Value = model.ContactDescription;

                if (!string.IsNullOrEmpty(slide1))
                {
                    dictionaries.Single((item) => item.Key == "Slide1").Value = "Content/upload/" + slide1;
                }

                if (!string.IsNullOrEmpty(slide2))
                {
                    dictionaries.Single((item) => item.Key == "Slide2").Value = "Content/upload/" + slide2;
                }

                if (!string.IsNullOrEmpty(slide3))
                {
                    dictionaries.Single((item) => item.Key == "Slide3").Value = "Content/upload/" + slide3;
                }

                context.SaveChanges();
            }

            return RedirectToAction("Index", "Admin");
        }

        //
        // GET: /Admin/Register

        [AllowAnonymous]
        public ActionResult Register()
        {
            return View();
        }

        //
        // POST: /Admin/Register

        [HttpPost]
        [AllowAnonymous]
        [ValidateAntiForgeryToken]
        public ActionResult Register(RegisterModel model)
        {
            if (ModelState.IsValid)
            {
                // Attempt to register the user
                try
                {
                    WebSecurity.CreateUserAndAccount(model.UserName, model.Password);
                    WebSecurity.Login(model.UserName, model.Password);
                    return RedirectToAction("Login", "Admin");
                }
                catch (MembershipCreateUserException e)
                {
                    ModelState.AddModelError("", ErrorCodeToString(e.StatusCode));
                }
            }

            // If we got this far, something failed, redisplay form
            return View(model);
        }

        #region Helpers
        private ActionResult RedirectToLocal(string returnUrl)
        {
            if (Url.IsLocalUrl(returnUrl))
            {
                return Redirect(returnUrl);
            }
            else
            {
                return RedirectToAction("Index", "Admin");
            }
        }

        private static string ErrorCodeToString(MembershipCreateStatus createStatus)
        {
            // See http://go.microsoft.com/fwlink/?LinkID=177550 for
            // a full list of status codes.
            switch (createStatus)
            {
                case MembershipCreateStatus.DuplicateUserName:
                    return "User name already exists. Please enter a different user name.";

                case MembershipCreateStatus.DuplicateEmail:
                    return "A user name for that e-mail address already exists. Please enter a different e-mail address.";

                case MembershipCreateStatus.InvalidPassword:
                    return "The password provided is invalid. Please enter a valid password value.";

                case MembershipCreateStatus.InvalidEmail:
                    return "The e-mail address provided is invalid. Please check the value and try again.";

                case MembershipCreateStatus.InvalidAnswer:
                    return "The password retrieval answer provided is invalid. Please check the value and try again.";

                case MembershipCreateStatus.InvalidQuestion:
                    return "The password retrieval question provided is invalid. Please check the value and try again.";

                case MembershipCreateStatus.InvalidUserName:
                    return "The user name provided is invalid. Please check the value and try again.";

                case MembershipCreateStatus.ProviderError:
                    return "The authentication provider returned an error. Please verify your entry and try again. If the problem persists, please contact your system administrator.";

                case MembershipCreateStatus.UserRejected:
                    return "The user creation request has been canceled. Please verify your entry and try again. If the problem persists, please contact your system administrator.";

                default:
                    return "An unknown error occurred. Please verify your entry and try again. If the problem persists, please contact your system administrator.";
            }
        }
        #endregion
    }
}
