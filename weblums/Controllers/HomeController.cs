﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Mail;
using System.Text;
using System.Web;
using System.Web.Mail;
using System.Web.Mvc;
using weblums.Models;

namespace weblums.Controllers
{
    public class HomeController : Controller
    {
        public ActionResult Index()
        {
            if (TempData.ContainsKey("Message"))
            {
                ViewBag.Message = TempData["Message"];
            }

            GuestModel model = new GuestModel();

            using (var context = new GuestsContext())
            {
                var dictionaries = context.Dictionaries.ToList();

                //descriptions
                model.Dashboard.WelcomeDescription = dictionaries.Single((item) => item.Key == "Welcome").Value;
                model.Dashboard.SpecialtiesDescription = dictionaries.Single((item) => item.Key == "Specialties").Value;
                model.Dashboard.MenuDescription = dictionaries.Single((item) => item.Key == "Menu").Value;
                model.Dashboard.ReservationDescription = dictionaries.Single((item) => item.Key == "Reservation").Value;
                model.Dashboard.ContactDescription = dictionaries.Single((item) => item.Key == "Contact").Value;

                //slides
                model.Dashboard.Slide1 = dictionaries.Single((item) => item.Key == "Slide1").Value;
                model.Dashboard.Slide2 = dictionaries.Single((item) => item.Key == "Slide2").Value;
                model.Dashboard.Slide3 = dictionaries.Single((item) => item.Key == "Slide3").Value;

                //specialties
                foreach (var specialty in context.Specialties.OrderBy((item) => item.DisplayIndex).Take(3))
                {
                    var item = new SpecialtyModel();
                    item.Id = specialty.Id;
                    item.Name = specialty.Name;
                    item.Description = specialty.Description;
                    item.ImageUrl = specialty.ImageUrl;
                    item.DisplayIndex = specialty.DisplayIndex;

                    model.Specialties.Add(item);
                }

                //menu
                foreach (var menu in context.Menu.ToList())
                {
                    var item = new MenuModel();
                    item.Id = menu.Id;
                    item.Name = menu.Name;
                    item.Description = menu.Description;
                    item.ImageUrl = menu.ImageUrl;
                    item.Price = menu.Price;
                    item.IsBreakfast = menu.IsBreakfast;
                    item.IsLunch = menu.IsLunch;
                    item.IsDinner = menu.IsDinner;
                    item.IsKids = menu.IsKids;
                    item.IsBeverage = menu.IsBeverage;
                    item.IsGallery = menu.IsGallery;

                    model.Menu.Add(item);
                }

            }

            return View(model);
        }

        //
        // POST: /Home/Reserve

        [HttpPost]
        [AllowAnonymous]
        public JsonResult Reserve(GuestModel model)
        {
            if (string.IsNullOrEmpty(model.Reservation.Name))
            {
                return Json(new { error = true, message = "Please enter contact name." }, JsonRequestBehavior.AllowGet);
            }

            if (string.IsNullOrEmpty(model.Reservation.Email))
            {
                return Json(new { error = true, message = "Please enter contact email." }, JsonRequestBehavior.AllowGet);
            }

            if (string.IsNullOrEmpty(model.Reservation.Phone))
            {
                return Json(new { error = true, message = "Please enter contact phone." }, JsonRequestBehavior.AllowGet);
            }

            if (!model.Reservation.Size.HasValue || model.Reservation.Size < 1)
            {
                return Json(new { error = true, message = "Please enter number of persons." }, JsonRequestBehavior.AllowGet);
            }

            if (!model.Reservation.Date.HasValue)
            {
                return Json(new { error = true, message = "Please enter date." }, JsonRequestBehavior.AllowGet);
            }

            if (string.IsNullOrEmpty(model.Reservation.Time))
            {
                return Json(new { error = true, message = "Please enter time." }, JsonRequestBehavior.AllowGet);
            }

            try
            {
                Reservations reservation = new Reservations();
                reservation.Name = model.Reservation.Name;
                reservation.Email = model.Reservation.Email;
                reservation.Phone = model.Reservation.Phone;
                reservation.Size = model.Reservation.Size.Value;
                reservation.Date = model.Reservation.Date.Value;
                reservation.Time = model.Reservation.Time;
                reservation.Note = model.Reservation.Note;
                reservation.isRead = false;
                reservation.isConfirmed = false;
                reservation.CreatedDate = DateTime.Now;

                if (reservation.Time.Length == 7)
                {
                    reservation.Time = "0" + reservation.Time;
                }

                using (var context = new GuestsContext())
                {
                    context.Reservations.Add(reservation);
                    context.SaveChanges();
                }

                StringBuilder sb = new StringBuilder();
                sb.AppendLine("Name: " + reservation.Name);
                sb.AppendLine("Email: " + reservation.Email);
                sb.AppendLine("Phone: " + reservation.Phone);
                sb.AppendLine("Number of Persons: " + reservation.Size);
                sb.AppendLine("Date: " + reservation.Date.ToShortDateString());
                sb.AppendLine("Time: " + reservation.Time);

                var SmtpClient = new SmtpClient();
                SmtpClient.Send("contact@lamsthebestgrill.com.au", "lamsthebestgrill@gmail.com", "New Reservation", sb.ToString());

                return Json(new { error = false, message = "Reserved succeeded." }, JsonRequestBehavior.AllowGet);
            }
            catch
            {
                return Json(new { error = true, message = "Reserved failed." }, JsonRequestBehavior.AllowGet);
            }
        }

        //
        // POST: /Home/Contact

        [HttpPost]
        [AllowAnonymous]
        public JsonResult Contact(GuestModel model)
        {
            if (string.IsNullOrEmpty(model.Contact.Name))
            {
                return Json(new { error = true, message = "Please enter contact name." }, JsonRequestBehavior.AllowGet);
            }

            if (string.IsNullOrEmpty(model.Contact.Email))
            {
                return Json(new { error = true, message = "Please enter contact email." }, JsonRequestBehavior.AllowGet);
            }

            if (string.IsNullOrEmpty(model.Contact.Message))
            {
                return Json(new { error = true, message = "Please enter message." }, JsonRequestBehavior.AllowGet);
            }

            try
            {
                Messages message = new Messages();
                message.Name = model.Contact.Name;
                message.Email = model.Contact.Email;
                message.Message = model.Contact.Message;
                message.isRead = false;
                message.CreatedDate = DateTime.Now;

                using (var context = new GuestsContext())
                {
                    context.Messages.Add(message);
                    context.SaveChanges();
                }

                StringBuilder sb = new StringBuilder();
                sb.AppendLine("Name: " + message.Name);
                sb.AppendLine("Email: " + message.Email);
                sb.AppendLine("Message: " + message.Message);

                var SmtpClient = new SmtpClient();
                SmtpClient.Send("contact@lamsthebestgrill.com.au", "lamsthebestgrill@gmail.com", "New Contact", sb.ToString());

                return Json(new { error = false, message = "Send message succeeded." }, JsonRequestBehavior.AllowGet);
            }
            catch
            {
                return Json(new { error = true, message = "Send message failed." }, JsonRequestBehavior.AllowGet);
            }
        }
    }
}
