﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Data.Entity;
using System.Globalization;
using System.Web;
using System.Web.Mvc;
using System.Web.Security;

namespace weblums.Models
{
    public class AdminContext : DbContext
    {
        public AdminContext()
            : base("DefaultConnection")
        {
        }

        public DbSet<UserProfile> UserProfiles { get; set; }
        public DbSet<Dictionary> Dictionaries { get; set; }
        public DbSet<Specialties> Specialties { get; set; }
        public DbSet<Menu> Menu { get; set; }
        public DbSet<Messages> Messages { get; set; }
        public DbSet<Reservations> Reservations { get; set; }
    }

    public class GuestsContext : DbContext
    {
        public GuestsContext()
            : base("DefaultConnection")
        {
        }

        public DbSet<Dictionary> Dictionaries { get; set; }
        public DbSet<Reservations> Reservations { get; set; }
        public DbSet<Messages> Messages { get; set; }
        public DbSet<Specialties> Specialties { get; set; }
        public DbSet<Menu> Menu { get; set; }
    }

    public class GuestModel
    {
        public GuestModel()
        {
            Dashboard = new DashboardModel();
            Reservation = new ReservationModel();
            Contact = new MessageModel();
            Specialties = new List<SpecialtyModel>();
            this.Menu = new List<MenuModel>();
        }

        public DashboardModel Dashboard { get; set; }
        public ReservationModel Reservation { get; set; }
        public MessageModel Contact { get; set; }
        public IList<SpecialtyModel> Specialties { get; set; }
        public IList<MenuModel> Menu { get; set; }
    }

    public class DashboardModel
    {
        [Display(Name = "Welcome Description")]
        public string WelcomeDescription { get; set; }

        [Display(Name = "Specialties Description")]
        public string SpecialtiesDescription { get; set; }

        [Display(Name = "Menu Description")]
        public string MenuDescription { get; set; }

        [Display(Name = "Reservation Description")]
        public string ReservationDescription { get; set; }

        [Display(Name = "Contact Description")]
        public string ContactDescription { get; set; }

        [Display(Name = "Slide 1")]
        public string Slide1 { get; set; }

        [Display(Name = "Slide 2")]
        public string Slide2 { get; set; }

        [Display(Name = "Slide 3")]
        public string Slide3 { get; set; }
    }

    public class RegisterExternalLoginModel
    {
        [Required]
        [Display(Name = "User name")]
        public string UserName { get; set; }

        public string ExternalLoginData { get; set; }
    }

    public class LocalPasswordModel
    {
        [Required]
        [DataType(DataType.Password)]
        [Display(Name = "Current password")]
        public string OldPassword { get; set; }

        [Required]
        [StringLength(100, ErrorMessage = "The {0} must be at least {2} characters long.", MinimumLength = 6)]
        [DataType(DataType.Password)]
        [Display(Name = "New password")]
        public string NewPassword { get; set; }

        [DataType(DataType.Password)]
        [Display(Name = "Confirm new password")]
        [Compare("NewPassword", ErrorMessage = "The new password and confirmation password do not match.")]
        public string ConfirmPassword { get; set; }
    }

    public class LoginModel
    {
        [Required]
        [Display(Name = "User name")]
        public string UserName { get; set; }

        [Required]
        [DataType(DataType.Password)]
        [Display(Name = "Password")]
        public string Password { get; set; }

        [Display(Name = "Remember me?")]
        public bool RememberMe { get; set; }
    }

    public class RegisterModel
    {
        [Required]
        [Display(Name = "User name")]
        public string UserName { get; set; }

        [Required]
        [StringLength(100, ErrorMessage = "The {0} must be at least {2} characters long.", MinimumLength = 6)]
        [DataType(DataType.Password)]
        [Display(Name = "Password")]
        public string Password { get; set; }

        [DataType(DataType.Password)]
        [Display(Name = "Confirm password")]
        [Compare("Password", ErrorMessage = "The password and confirmation password do not match.")]
        public string ConfirmPassword { get; set; }
    }

    public class SpecialtyModel
    {
        [Display(Name = "Id")]
        public int Id { get; set; }

        [Display(Name = "Name")]
        public string Name { get; set; }

        [Display(Name = "Description")]
        public string Description { get; set; }

        [Display(Name = "ImageUrl")]
        public string ImageUrl { get; set; }

        [Display(Name = "DisplayIndex")]
        public int? DisplayIndex { get; set; }
    }

    public class MenuModel
    {
        [Display(Name = "Id")]
        public int Id { get; set; }

        [Display(Name = "Name")]
        public string Name { get; set; }

        [Display(Name = "Description")]
        public string Description { get; set; }

        [Display(Name = "ImageUrl")]
        public string ImageUrl { get; set; }

        [Display(Name = "Price")]
        public decimal? Price { get; set; }

        [Display(Name = "IsBreakfast")]
        public bool IsBreakfast { get; set; }

        [Display(Name = "IsLunch")]
        public bool IsLunch { get; set; }

        [Display(Name = "IsDinner")]
        public bool IsDinner { get; set; }

        [Display(Name = "IsKids")]
        public bool IsKids { get; set; }

        [Display(Name = "IsBeverage")]
        public bool IsBeverage { get; set; }

        [Display(Name = "IsGallery")]
        public bool IsGallery { get; set; }
    }

    public class ReservationModel
    {
        [Display(Name = "Id")]
        public int Id { get; set; }

        [Display(Name = "Name")]
        public string Name { get; set; }

        [Display(Name = "Email")]
        public string Email { get; set; }

        [Display(Name = "Phone")]
        public string Phone { get; set; }

        [Display(Name = "Size")]
        public int? Size { get; set; }

        [Display(Name = "CreatedDate")]
        public DateTime? CreatedDate { get; set; }

        [Display(Name = "Date")]
        public DateTime? Date { get; set; }

        [Display(Name = "Time")]
        public string Time { get; set; }

        [Display(Name = "Note")]
        public string Note { get; set; }

        [Display(Name = "isRead")]
        public bool isRead { get; set; }

        [Display(Name = "isConfirmed")]
        public bool isConfirmed { get; set; }
    }

    public class MessageModel
    {
        [Display(Name = "Id")]
        public int Id { get; set; }

        [Display(Name = "Name")]
        public string Name { get; set; }

        [Display(Name = "Email")]
        public string Email { get; set; }

        [Display(Name = "Message")]
        public string Message { get; set; }

        [Display(Name = "isRead")]
        public bool isRead { get; set; }

        [Display(Name = "CreatedDate")]
        public DateTime? CreatedDate { get; set; }
    }

    public class ExternalLogin
    {
        public string Provider { get; set; }
        public string ProviderDisplayName { get; set; }
        public string ProviderUserId { get; set; }
    }

    [Table("UserProfile")]
    public class UserProfile
    {
        [Key]
        [DatabaseGeneratedAttribute(DatabaseGeneratedOption.Identity)]
        public int UserId { get; set; }
        public string UserName { get; set; }
    }

    [Table("Dictionary")]
    public class Dictionary
    {
        [Key]
        public string Key { get; set; }
        public string Value { get; set; }
    }

    [Table("Specialties")]
    public class Specialties
    {
        [Key]
        [DatabaseGeneratedAttribute(DatabaseGeneratedOption.Identity)]
        public int Id { get; set; }
        public string Name { get; set; }
        public string Description { get; set; }
        public string ImageUrl { get; set; }
        public int? DisplayIndex { get; set; }
    }

    [Table("Menu")]
    public class Menu
    {
        [Key]
        [DatabaseGeneratedAttribute(DatabaseGeneratedOption.Identity)]
        public int Id { get; set; }
        public string Name { get; set; }
        public string Description { get; set; }
        public string ImageUrl { get; set; }
        public decimal? Price { get; set; }
        public bool IsGallery { get; set; }
        public bool IsBreakfast { get; set; }
        public bool IsLunch { get; set; }
        public bool IsDinner { get; set; }
        public bool IsKids { get; set; }
        public bool IsBeverage { get; set; }
    }

    [Table("Messages")]
    public class Messages
    {
        [Key]
        [DatabaseGeneratedAttribute(DatabaseGeneratedOption.Identity)]
        public int Id { get; set; }
        public string Name { get; set; }
        public string Email { get; set; }
        public string Message { get; set; }
        public bool? isRead { get; set; }
        public DateTime CreatedDate { get; set; }
    }

    [Table("Reservations")]
    public class Reservations
    {
        [Key]
        [DatabaseGeneratedAttribute(DatabaseGeneratedOption.Identity)]
        public int Id { get; set; }
        public string Name { get; set; }
        public string Email { get; set; }
        public string Phone { get; set; }
        public int Size { get; set; }
        public DateTime CreatedDate { get; set; }
        public DateTime Date { get; set; }
        public string Time { get; set; }
        public string Note { get; set; }
        public bool? isRead { get; set; }
        public bool? isConfirmed { get; set; }
    }
}
