﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using System.Web.Routing;

namespace weblums
{
    public class RouteConfig
    {
        public static void RegisterRoutes(RouteCollection routes)
        {
            routes.IgnoreRoute("{resource}.axd/{*pathInfo}");

            routes.MapRoute(
                name: "Login",
                url: "login",
                defaults: new { controller = "Admin", action = "Login", id = UrlParameter.Optional }
            );

            routes.MapRoute(
                name: "Register",
                url: "register",
                defaults: new { controller = "Admin", action = "Register", id = UrlParameter.Optional }
            );

            routes.MapRoute(
                name: "Admin",
                url: "admin",
                defaults: new { controller = "Admin", action = "Index", id = UrlParameter.Optional }
            );

            routes.MapRoute(
                name: "Notification",
                url: "admin/notification",
                defaults: new { controller = "Admin", action = "Notification", id = UrlParameter.Optional }
            );

            routes.MapRoute(
                name: "Specialties.Add",
                url: "admin/specialties/add",
                defaults: new { controller = "Admin", action = "AddSpecialty", id = UrlParameter.Optional }
            );

            routes.MapRoute(
                name: "Specialties.Edit",
                url: "admin/specialties/edit",
                defaults: new { controller = "Admin", action = "EditSpecialty", id = UrlParameter.Optional }
            );

            routes.MapRoute(
                name: "Specialties.Delete",
                url: "admin/specialties/delete",
                defaults: new { controller = "Admin", action = "DeleteSpecialty", id = UrlParameter.Optional }
            );

            routes.MapRoute(
                name: "Menu.Add",
                url: "admin/menu/add",
                defaults: new { controller = "Admin", action = "AddMenu", id = UrlParameter.Optional }
            );

            routes.MapRoute(
                name: "Menu.Edit",
                url: "admin/menu/edit",
                defaults: new { controller = "Admin", action = "EditMenu", id = UrlParameter.Optional }
            );

            routes.MapRoute(
                name: "Menu.Delete",
                url: "admin/menu/delete",
                defaults: new { controller = "Admin", action = "DeleteMenu", id = UrlParameter.Optional }
            );

            routes.MapRoute(
                name: "Message.View",
                url: "admin/messages/view",
                defaults: new { controller = "Admin", action = "ViewMessage", id = UrlParameter.Optional }
            );

            routes.MapRoute(
                name: "Message.Delete",
                url: "admin/messages/delete",
                defaults: new { controller = "Admin", action = "DeleteMessage", id = UrlParameter.Optional }
            );

            routes.MapRoute(
                name: "Reservations.Edit",
                url: "admin/reservations/edit",
                defaults: new { controller = "Admin", action = "EditReservation", id = UrlParameter.Optional }
            );

            routes.MapRoute(
                name: "Reservations.Delete",
                url: "admin/reservations/delete",
                defaults: new { controller = "Admin", action = "DeleteReservation", id = UrlParameter.Optional }
            );

            routes.MapRoute(
                name: "Default",
                url: "{controller}/{action}/{id}",
                defaults: new { controller = "Home", action = "Index", id = UrlParameter.Optional }
            );
        }
    }
}