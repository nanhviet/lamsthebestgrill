﻿using System.Web;
using System.Web.Optimization;

namespace weblums
{
    public class BundleConfig
    {
        // For more information on Bundling, visit http://go.microsoft.com/fwlink/?LinkId=254725
        public static void RegisterBundles(BundleCollection bundles)
        {
            bundles.Add(new ScriptBundle("~/Content/scripts").Include(
                        "~/Content/js/jquery.js",
                        "~/Content/js/modernizr.js",
                        "~/Content/js/bootstrap.js",
                        "~/Content/js/bootstrap.modal.js",
                        "~/Content/js/jquery.scrollTo.js",
                        "~/Content/js/jquery.nav.js",
                        "~/Content/js/owl.carousel.js",
                        "~/Content/js/jquery.flexslider.js",
                        "~/Content/js/jquery.accordion.js",
                        "~/Content/js/jquery.placeholder.js",
                        "~/Content/js/jquery.fitvids.js",
                        "~/Content/js/masonry.js",
                        "~/Content/js/moment.js",
                    //"~/Content/js/bootstrap-datetimepicker.js",
                        "~/Content/vendor/pickadate/picker.js",
                        "~/Content/vendor/pickadate/picker.date.js",
                        "~/Content/vendor/pickadate/picker.time.js",
                        "~/Content/js/classie.js",
                        "~/Content/js/imgloaded.js",
                        "~/Content/js/gridscroll.js",
                        "~/Content/js/gmap3.js",
                        "~/Content/js/fancySelect.js",
                        "~/Content/js/main.js"));

            bundles.Add(new StyleBundle("~/Content/styles").Include(
                        "~/Content/css/bootstrap.css",
                        "~/Content/css/bootstrap.modal.css",
                        "~/Content/css/style.css",
                        "~/Content/css/animate.min.css",
                        "~/Content/font-awesome/css/font-awesome.css",
                        "~/Content/css/owl.carousel.css",
                        //"~/Content/css/bootstrap-datetimepicker.css",
                        "~/Content/vendor/pickadate/themes/default.css",
                        "~/Content/vendor/pickadate/themes/default.date.css",
                        "~/Content/vendor/pickadate/themes/default.time.css",
                        "~/Content/css/flexslider.css",
                        "~/Content/css/fancySelect.css",
                        "~/Content/css/responsive.css"));

            bundles.Add(new ScriptBundle("~/Content/adminScripts").Include(
                        "~/Content/js/jquery.js",
                        "~/Content/js/bootstrap.js",
                        "~/Content/js/bootstrap.modal.js",
                        "~/Content/js/underscore.min.js",
                        "~/Content/js/holder.js"));

            bundles.Add(new StyleBundle("~/Content/adminStyles").Include(
                        "~/Content/css/bootstrap.css",
                        "~/Content/css/bootstrap.modal.css",
                        "~/Content/css/admin.css"));

        }
    }
}